---
title: "RawTherapee v4.2"
date: 2014-10-24T21:52:00+00:00
author: DrSlony
draft: false
version: ""
linux_appimage: ""
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://rawtherapee.com/shared/builds/windows/RawTherapee_WinVista_64_4.2.1.zip"
windows_xp_binary: "https://rawtherapee.com/shared/builds/windows/RawTherapee_WinXP_32_4.2.1.zip"
mac_binary: "https://rawtherapee.com/shared/builds/mac/RawTherapee_OSX_10.10_64_4.2.1.zip"
source_code: "https://rawtherapee.com/shared/source/rawtherapee-4.2.tar.xz"
---


RawTherapee 4.2 is out, and if you're not using it in 5 minutes then you're not one of the cool kids.

{{< figure src="rt_splash_4.2.png" >}}


## New features

- RawTherapee-4.2 includes [many](https://code.google.com/p/rawtherapee/source/list?name=4.2 "List of source code changes on Google Code") speed, precision, stability and memory usage optimizations. As such, users of 32-bit operating systems may now find that they can enjoy more stability while using the most memory intensive tools. Of course users of 64-bit
systems benefit from this as well. Refer to the full changelog for more information.
- Powerful [color toning](http://rawpedia.rawtherapee.com/Color_Toning "Color Toning page on RawPedia") tool.
- [Curve control](http://rawpedia.rawtherapee.com/Noise_Reduction#Luminance_Curve "Noise Reduction tool page's Luminance Curve section on RawPedia") of
- [Median filter](http://rawpedia.rawtherapee.com/Noise_Reduction#Median "Noise Reduction tool page's Median Filter section on RawPedia") in
- [Film simulation](http://rawpedia.rawtherapee.com/Film_Simulation "Film Simulation tool's page on RawPedia") tool using Hald CLUT pattern files.
- [Command-line option](http://rawpedia.rawtherapee.com/Command-Line_Options "Command Line page on RawPedia") to define bit depth of output TIFF/PNG file.
- Multiple improvements to [dead/hot pixel](http://rawpedia.rawtherapee.com/Preprocessing "Preprocessing page on RawPedia") handling, see RawPedia.
- Filename of currently opened image shown in the titlebar.
- [Clip control](http://rawpedia.rawtherapee.com/Flat_Field#Clip_Control "Flat Field tool page's Clip Control section on RawPedia") for the flat-field correction tool.
- [Demosaic](http://rawpedia.rawtherapee.com/Demosaicing "Demosaicing page on RawPedia") method "Mono" for monochrome cameras, and "None" for no demosaicing.
- Copy/paste processing profile [keyboard shortcuts](http://rawpedia.rawtherapee.com/Keyboard_Shortcuts "Keyboard Shortcuts page on RawPedia") for right-handed users using Ctrl/Shift-Insert.
- Update to dcraw 9.22 1.467
- New or improved support for:
    - Canon EOS 7D
    - Canon EOS 7D Mark II
    - Canon PowerShot G7 X
    - Canon PowerShot SX60 HS
    - Fujifilm cameras using the X-Trans sensor
    - Fujifilm X30
    - Hasselblad H4D-31
    - Hasselblad H4D-50
    - Hasselblad H4D-60
    - Hasselblad H5D-40
    - Hasselblad H5D-50c
    - Mamiya Leaf Credo 40
    - Mamiya Leaf Credo 50
    - Mamiya Leaf Credo 60
    - Mamiya Leaf Credo 80
    - Monochrome cameras such as Leica Monochrome
    - Nikon D610
    - Nikon D700
    - Nikon D750
    - Nikon D800E
    - Nikon D810
    - Panasonic Lumix DMC-FZ1000
    - Panasonic Lumix DMC-GM5
    - Panasonic Lumix DMC-LX100
    - Phase One IQ250
    - Phase One P40
    - Phase One P65+
    - Sony Alpha ILCE-5100
    - Sony NEX-C3
