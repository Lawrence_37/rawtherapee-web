---
title: "RawTherapee 5.9 Released"
date: 2022-11-27T22:00:00+01:00
author: DrSlony
draft: false
---

RawTherapee 5.9 has been released! Head over to the [Downloads](/downloads/5.9/) page to get it and to read the release notes.
