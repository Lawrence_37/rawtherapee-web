---
title: "A quick note on the word 'raw' and LittleCMS version 2"
date: 2011-06-23T23:07:00+00:00
author: DrSlony
Tags:
- Downloads
- Nightly Builds
- Versions
- RT 3.0
- RT 3.1
---


I would like to point two unrelated things out:

1.  The word "raw" is a plain word. It is not an acronym or an
    initialism. As such, the usual noun
    [capitalization](http://en.wikipedia.org/wiki/Capitalization "Wikipedia article on capitalization")
    rules apply. Writing *RAW* is incorrect. If starting a sentence,
    capitalize the first letter, leave the other two in lowercase. If in
    the middle of a sentence, all letters should be lowercase.
2.  The unstable RawTherapee-3.1 builds require
    [LittleCMS](http://www.littlecms.com/download.html "LittleCMS download page")
    version 2. Ubuntu users should see the guide in the forum on [how to
    install LCMS2 in
    Ubuntu](http://www.rawtherapee.com/forum/viewtopic.php?t=2879 "How to install LCMS2 in Ubuntu").
    RawTherapee-3.0 builds require LittleCMS version 1.

