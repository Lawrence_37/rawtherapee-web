---
title: "New RawTherapee 5.4 AppImage"
date: 2018-08-31T19:12:00+00:00
author: DrSlony
---



Linux users whose distributions do not ship RawTherapee 5.4 can now
enjoy our latest stable release by getting the RawTherapee 5.4 AppImage
from our Downloads page.


An AppImage is a single file which contains all the components necessary
to run a program in any Linux distribution. As some distributions
provide old versions of certain programs, the AppImage is a great
solution for letting users of those systems run the latest version of a
given program.

All you need to do after downloading it is to make it executable and run
it:

    chmod +x RawTherapee-releases-5.4.AppImage
    ./RawTherapee-releases-5.4.AppImage

