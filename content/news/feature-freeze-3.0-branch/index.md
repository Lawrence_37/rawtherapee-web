---
title: "Feature Freeze - 3.0 branch"
date: 2010-11-28T13:55:00+00:00
author: dualon
Tags:
- RT 3.0
---



We are happy to announce the feature freeze of RawTherapee 3.0\!

A [feature
freeze](http://en.wikipedia.org/wiki/Freeze_%28software_engineering%29)
is a point in the development process when no more new features are
accepted for the current branch. In accordance with the RT 3.0 Roadmap
the project arrived to this point.

Branching

We would like to call the attention -
especially of the developers - to the changes in the [source
tree](http://code.google.com/p/rawtherapee/source/list):

  - '3.0\_branch' is the new branch for the upcoming stable release;
    accepts only bug fixes and GUI enhancements. Please do not push new
    features here\!
  - 'default' is the continuation of the main branch; new features can
    be pushed here, although the efforts should be focused on the
    bugfixes now.

The fixes from the 3.0\_branch will be introduced later to the default
branch, but noone is restricted in any way to do it regularly and/or per
wish.

Testing

Everyone is invited to test and [report
bugs](http://code.google.com/p/rawtherapee/issues/entry), please check
the [list of known
issues](http://code.google.com/p/rawtherapee/issues/list?can=2&q=&sort=-id&colspec=ID%20Type%20Status%20Priority%20Milestone%20Owner%20Summary).
The latest builds are available from the /releases\_head directory as
always (will be updated with the current 3.0\_branch very soon):

<http://www.rawtherapee.com/shared/builds/>

Please note that these are alpha builds\! Use them on your own risk.

Have fun with testing and bug hunting - help us to release the stable
version as soon as possible\!

---

dualon

