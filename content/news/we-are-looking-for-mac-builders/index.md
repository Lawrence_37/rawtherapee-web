---
title: "We are looking for Mac builders"
date: 2012-08-19T11:19:00+00:00
author: dualon
Tags:
- Community
---



We are looking for Mac builders.  
  
RawTherapee is a cross-platform raw development tool, but due to the
lack of Mac builders our latest release on this platform is 4.0.6
(compared to the [currently
planned 4.1](http://code.google.com/p/rawtherapee/issues/detail?id=1419)).  
  
Requirements:  

  - minimal experience in cmake and compiling C++ source code on
    Macintosh platform
  - regular availability (e.g. willing to follow the project and build
    prior to releases)
  - access to multiple versions of Mac OS X is a plus, but not a must
    have  

  
No special skills or knowledge is needed beyond that. It is OK if you
are not familiar with RawTherapee's build background: we will give you
all the neccessary information if needed.  
You don't have to do anything special, just announce your intention in
[Issue 1526](http://code.google.com/p/rawtherapee/issues/detail?id=1526)\!  

