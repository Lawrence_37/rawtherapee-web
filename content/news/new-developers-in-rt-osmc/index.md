---
title: "New Developers in RT OSMC"
date: 2010-11-22T00:33:00+00:00
author: dualon
Tags:
- Community
- OSMC
---



Based on your feedback and the outstanding activity of Hombre and Emil,
both of them were invited into the RT OSMC\! We are glad to announce
that the current line-up of the team is (in alphabetical order):

  - DrSlony,
  -  dualon,
  -  ejmartin,
  -  Gabor,
  -  Hombre,
  -  janrinze,
  -  paul.matthijsse,
  -  TheBigOne.

Best wishes to you, guys, and thanks for all your efforts\!

