---
title: Design
date: 2018-08-23T21:27:35-05:00
draft: false
---

Thanks to Mica we have a basic [Bootstrap 4][] theme running.
Literally the most basic framework implemented (which is a good thing, imo).

[Bootstrap 4]: https://getbootstrap.com/docs/4.0 "Bootstrap docs"


## Typography

## Colors


## Grid-stuff

I don't have a ton of experience with using Bootstraps flexbox stuff for layout, but I'm getting it. :)

For reference, the layout stuff is here: https://getbootstrap.com/docs/4.0/layout/grid/
